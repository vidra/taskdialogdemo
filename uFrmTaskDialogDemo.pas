unit uFrmTaskDialogDemo;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls;

type
  TFrmTaskDialogDemo = class(TForm)
    pblControls: TPanel;
    btnZatvori: TButton;
    btnBasicDemo: TButton;
    btnTaskMessageDlg: TButton;
    btnTaskDialogPosition: TButton;
    TaskDialog1: TTaskDialog;
    btnKomponenta: TButton;
    memTekstovi: TMemo;
    Label1: TLabel;
    Label2: TLabel;
    edtTitle: TEdit;
    edtCaption: TEdit;
    btnObrisiTekst: TButton;
    Label3: TLabel;
    Label4: TLabel;
    edtX: TEdit;
    edtY: TEdit;
    Label5: TLabel;
    edtText: TEdit;
    cbExtraText: TCheckBox;
    cbBtnOk: TCheckBox;
    cbBtnYes: TCheckBox;
    cbBtnNo: TCheckBox;
    cbBtnCancel: TCheckBox;
    cbBtnRetry: TCheckBox;
    cbBtnClose: TCheckBox;
    btnDemoButtons: TButton;
    btnKomponentaBotuni: TButton;
    gbButtons: TGroupBox;
    gbFlags: TGroupBox;
    cbEnableHyperlinks: TCheckBox;
    cbUseHIconMain: TCheckBox;
    cbUseHIconFooter: TCheckBox;
    cbAllowDialogCancellation: TCheckBox;
    cbUseCommandLinks: TCheckBox;
    cbUseCommandLinksNoIcon: TCheckBox;
    cbExpandFooterArea: TCheckBox;
    cbExpandedByDefault: TCheckBox;
    cbRtlLayout: TCheckBox;
    cbCanBeMinimized: TCheckBox;
    cbNoDefaultRadioButton: TCheckBox;
    cbPositionRelativeToWindow: TCheckBox;
    cbShowProgressBar: TCheckBox;
    cbCallbackTimer: TCheckBox;
    cbShowMarqueeProgressBar: TCheckBox;
    cbVerificationFlagChecked: TCheckBox;
    cbIcons: TComboBox;
    Label6: TLabel;
    cbDefaultButton: TComboBox;
    Label7: TLabel;
    cbVerification: TCheckBox;
    cbRadioButtons: TCheckBox;
    Label8: TLabel;
    edtFooterText: TEdit;
    Label9: TLabel;
    cbFooterIcon: TComboBox;
    Label10: TLabel;
    edtMarquee: TEdit;
    Label11: TLabel;
    Label12: TLabel;
    cbProgressType: TComboBox;
    Label13: TLabel;
    edtProgressPosition: TEdit;
    Label14: TLabel;
    edtProgressMin: TEdit;
    Label15: TLabel;
    edtProgressMax: TEdit;
    btnAPI: TButton;
    btnProgress: TButton;
    procedure btnZatvoriClick(Sender: TObject);
    procedure btnBasicDemoClick(Sender: TObject);
    procedure btnTaskMessageDlgClick(Sender: TObject);
    procedure btnTaskDialogPositionClick(Sender: TObject);
    procedure btnObrisiTekstClick(Sender: TObject);
    procedure btnKomponentaClick(Sender: TObject);
    procedure btnDemoButtonsClick(Sender: TObject);
    procedure btnKomponentaBotuniClick(Sender: TObject);
    procedure TaskDialog1HyperlinkClicked(Sender: TObject);
    procedure TaskDialog1Navigated(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cbBtnOkClick(Sender: TObject);
    procedure cbBtnYesClick(Sender: TObject);
    procedure cbBtnNoClick(Sender: TObject);
    procedure cbBtnCancelClick(Sender: TObject);
    procedure cbBtnCloseClick(Sender: TObject);
    procedure cbBtnRetryClick(Sender: TObject);
    procedure TaskDialogButtonClicked(Sender: TObject;
      ModalResult: TModalResult; var CanClose: Boolean);
    procedure TaskDialogVerificationClicked(Sender: TObject);
    procedure OnRadioButtonClicked(Sender: TObject);
    procedure TaskDialog1Timer(Sender: TObject; TickCount: Cardinal;
      var Reset: Boolean);
    procedure tdProgressButtonClicked(Sender: TObject;
      ModalResult: TModalResult; var CanClose: Boolean);
    procedure tdProgressTimer(Sender: TObject; TickCount: Cardinal;
      var Reset: Boolean);
    procedure btnProgressClick(Sender: TObject);
    procedure btnAPIClick(Sender: TObject);
  private
    FFinished: Boolean;
    FCurrNumber: Integer;
    FPrimeNumbersCount: Integer;
    procedure ObradiModalRezultat(AModalResult: TModalResult);
    procedure OsnovniTaskDialog(var TaskDialog: TTaskDialog);
    procedure AddButtons(TaskDialog: TTaskDialog);
    procedure SetTaskDialogButtonSet(var ButtonSet: TTaskDialogCommonButtons);
    procedure SetTaskDialogFlags(var FlagsSet: TTaskDialogFlags);
    procedure SetComponentDefaultButton(var TaskDialog: TTaskDialog);
    procedure SetTaskDialogVerification(var TaskDialog: TTaskDialog);
    procedure ExecuteTaskDialog(TaskDialog: TTaskDialog);
    function IsPrimeNumber(Number: Integer): Boolean;
    { Private declarations }
  public
    { Public declarations }
  end;

const
  GOOGLE_SEARCH = 99;
  MAX_NUMBERS = 1000;
  NUMBERS_IN_A_SINGLE_STEP = 50;

var
  FrmTaskDialogDemo: TFrmTaskDialogDemo;

implementation

{$R *.dfm}

uses System.UITypes
  , ShellApi
  , VCL.Themes
  , uAnonProcToNotify
  , Winapi.CommCtrl
  ;

procedure TFrmTaskDialogDemo.btnBasicDemoClick(Sender: TObject);
var
  TaskDialog: TTaskDialog;
  Button: TTaskDialogBaseButtonItem;
begin
  memTekstovi.Lines.Insert(0, 'Dinami�ko kreiranje TTaskDialog objekta u kodu');
  memTekstovi.Lines.Insert(0, 'TaskDialog.ModalResult := MB_OK');
  memTekstovi.Lines.Insert(0, 'http://docwiki.embarcadero.com/CodeExamples/Sydney/en/TaskDialogs_(Delphi)');

  // create a custom task dialog
  TaskDialog := TTaskDialog.Create(Self);
  try
    OsnovniTaskDialog(TaskDialog);
    ExecuteTaskDialog(TaskDialog);
    ObradiModalRezultat(TaskDialog.ModalResult);
  finally
    FreeAndNil(TaskDialog);
  end;

end;

procedure TFrmTaskDialogDemo.btnDemoButtonsClick(Sender: TObject);
var
  TaskDialog: TTaskDialog;
begin
  memTekstovi.Lines.Insert(0, 'Dinami�ko kreiranje TTaskDialog objekta sa botunima u kodu');
  memTekstovi.Lines.Insert(0, 'TaskDialog.ModalResult := MB_OK');
  memTekstovi.Lines.Insert(0, 'Button.ModalResult := MB_OK');
  memTekstovi.Lines.Insert(0, 'Button.ModalResult := MB_RETRYCANCEL');
  memTekstovi.Lines.Insert(0, 'http://docwiki.embarcadero.com/CodeExamples/Sydney/en/TaskDialogs_(Delphi)');

  // create a custom task dialog
  TaskDialog := TTaskDialog.Create(Self);
  try
    OsnovniTaskDialog(TaskDialog);

    AddButtons(TaskDialog); // Winapi Windows

    ExecuteTaskDialog(TaskDialog);
    ObradiModalRezultat(TaskDialog.ModalResult);
  finally
    FreeAndNil(TaskDialog);
  end;
end;

procedure TFrmTaskDialogDemo.btnKomponentaClick(Sender: TObject);
var
  ButtonSet: TTaskDialogCommonButtons;
  FlagsSet: TTaskDialogFlags;
begin

  OsnovniTaskDialog(TaskDialog1);
  ExecuteTaskDialog(TaskDialog1);
  ObradiModalRezultat(TaskDialog1.ModalResult);
end;

procedure TFrmTaskDialogDemo.btnObrisiTekstClick(Sender: TObject);
begin
  memTekstovi.Lines.Clear;
end;

procedure TFrmTaskDialogDemo.ObradiModalRezultat(AModalResult: TModalResult);
var
  LGSearch: String;
begin
  // Vcl.Dialogs.TCustomTaskDialog.ModalResult:
  // ModalResult contains the modal result of the Task Dialog:
  // mrYes, mrNo, mrOk, mrCancel mrAbort, mrRetry, mrIgnore, mrAll, mrNoToAll, mrYesToAll, mrClose.
  case AModalResult of
    mrNone:
      memTekstovi.Lines.Insert(0, '0  None. Used as a default value before the user exits. ');
    mrOk:
      memTekstovi.Lines.Insert(0, 'idOK = 1 The user exited with the OK button.');
    mrCancel:
      memTekstovi.Lines.Insert(0, 'idCancel = 2 The user exited with the CANCEL button.');
    mrAbort:
      memTekstovi.Lines.Insert(0, 'idAbort = 3 The user exited with the ABORT button.');
    mrRetry:
      memTekstovi.Lines.Insert(0, 'idRetry = 4 The user exited with the RETRY button.');
    mrIgnore:
      memTekstovi.Lines.Insert(0, 'idIgnore = 5 The user exited with the IGNORE button.');
    mrYes:
      memTekstovi.Lines.Insert(0, 'idYes = 6 The user exited with the YES button.');
    mrNo:
      memTekstovi.Lines.Insert(0, 'idNo = 7 The user exited with the NO button.');
    mrClose:
      memTekstovi.Lines.Insert(0, 'idClose = 8 The user exited with the CLOSE button.');
    mrHelp:
      // System.UITypes
      memTekstovi.Lines.Insert(0, 'idHelp = 9 The user exited with the HELP button.');
    mrTryAgain:
      // System.UITypes
      memTekstovi.Lines.Insert(0, 'idTryAgain = 10 The user exited with the TRY AGAIN button.');
    mrContinue:
      // System.UITypes
      memTekstovi.Lines.Insert(0, 'idContinue = 11 The user exited with the CONTINUE button.');
    mrAll:
      memTekstovi.Lines.Insert(0, 'mrContinue + 1 (12 or $C) The user exited with the ALL button.');
    mrNoToAll:
      memTekstovi.Lines.Insert(0, 'mrAll +1 (13 or $D) The user exited with the NO TO ALL button.');
    mrYesToAll:
      memTekstovi.Lines.Insert(0, 'mrNoToAll +1 (14 or $E) The user exited with the YES TO ALL button.');
  else
    memTekstovi.Lines.Insert(0, 'Nepoznati System.UITypes modal result. Kako to?');
  end;
  memTekstovi.Lines.Insert(0, 'Modal Result according to System.UITypes ');
  case AModalResult of
    MB_OK:
      memTekstovi.Lines.Insert(0, 'MB_OK = $00000000');
    MB_OKCANCEL:
      memTekstovi.Lines.Insert(0, 'MB_OKCANCEL = $00000001');
    MB_ABORTRETRYIGNORE:
      memTekstovi.Lines.Insert(0, 'MB_ABORTRETRYIGNORE = $00000002');
    MB_YESNOCANCEL:
      memTekstovi.Lines.Insert(0, 'MB_YESNOCANCEL = $00000003');
    MB_YESNO:
      memTekstovi.Lines.Insert(0, 'MB_YESNO = $00000004');
    MB_RETRYCANCEL:
      memTekstovi.Lines.Insert(0, 'MB_RETRYCANCEL = $00000005');
    GOOGLE_SEARCH:
    begin
      memTekstovi.Lines.Insert(0, 'GOOGLE_SEARCH = 99');
      LGSearch := Format('https://www.google.com/search?q=%s', ['TTaskDialog+Delphi']);
      ShellExecute(0, 'open', PChar(LGSearch), nil, nil, SW_SHOWNORMAL);
    end
  else
    memTekstovi.Lines.Insert(0, 'Nepoznati Winapi Windows modal result. Kako to?');
  end;
  memTekstovi.Lines.Insert(0, 'Modal Result according to Winapi Windows ');
end;

procedure TFrmTaskDialogDemo.OsnovniTaskDialog(var TaskDialog: TTaskDialog);
var
  ButtonSet: TTaskDialogCommonButtons;
  FlagsSet: TTaskDialogFlags;
begin

  TaskDialog.Title := edtTitle.Text;
  TaskDialog.Caption := edtCaption.Text;
  TaskDialog.Text := edtText.Text;

  if cbExtraText.Checked then
    TaskDialog.ExpandedText := memTekstovi.Lines.Text
  else
    TaskDialog.ExpandedText := '';

  TaskDialog.CommonButtons := [];
  SetTaskDialogButtonSet(ButtonSet);
  TaskDialog.CommonButtons := ButtonSet;

  TaskDialog.OnHyperlinkClicked :=
    uAnonProcToNotify.AnonProc2NotifyEvent(TaskDialog,
                          procedure (Sender: TObject)
                            begin
                              if Sender is TTaskDialog then
                                with Sender as TTaskDialog do
                                  ShellExecute(0, 'open', PChar(URL), nil, nil, SW_SHOWNORMAL);
                            end
                          );

  TaskDialog.Flags := [];
  SetTaskDialogFlags(FlagsSet);
  TaskDialog.Flags := FlagsSet;

  TaskDialog.VerificationText := '';
  TaskDialog.OnVerificationClicked := nil;
  if cbVerification.Checked then SetTaskDialogVerification(TaskDialog);

  TaskDialog.MainIcon := cbIcons.ItemIndex;
  TaskDialog.FooterIcon := cbFooterIcon.ItemIndex;
  TaskDialog.FooterText := edtFooterText.Text;
  TaskDialog.CustomMainIcon.LoadFromFile('Icon.ico');
  TaskDialog.CustomFooterIcon.LoadFromFile('Icon.ico');

  TaskDialog.RadioButtons.Clear;
  if cbRadioButtons.Checked then
  begin
    TaskDialog.RadioButtons.Add.Caption := 'Radio 1';
    TaskDialog.RadioButtons.Add.Caption := 'Radio 2';
    TaskDialog.RadioButtons.Add.Caption := 'Radio 3';
    TaskDialog.OnRadioButtonClicked := OnRadioButtonClicked;
  end;
  SetComponentDefaultButton(TaskDialog);

  TaskDialog.ProgressBar.Min := StrToInt(edtProgressMin.Text);
  TaskDialog.ProgressBar.Max := StrToInt(edtProgressMax.Text);
  TaskDialog.ProgressBar.Position := StrToInt(edtProgressPosition.Text);
  if cbProgressType.ItemIndex = -1 then
    TaskDialog.ProgressBar.State := TProgressBarState.pbsNormal
  else
    TaskDialog.ProgressBar.State := TProgressBarState(cbProgressType.ItemIndex);
  TaskDialog.ProgressBar.MarqueeSpeed := StrToInt(edtMarquee.Text);

  TaskDialog.OnTimer := TaskDialog1Timer;
  // assign a MB_OK modal result to the task dialog
  TaskDialog.ModalResult := MB_OK;

end;

procedure TFrmTaskDialogDemo.AddButtons(TaskDialog: TTaskDialog);
var
  Button: TTaskDialogBaseButtonItem;
begin
  // Winapi Windows
  // add some customized buttons to the task dialog
  Button := TaskDialog.Buttons.Add;
  Button.Caption := 'Nastavi';
  Button.ModalResult := MB_OK;
  TTaskDialogButtonItem(Button).CommandLinkHint := 'Nastavi sa operacijom i nemoj ugasiti prozor.';
  // Winapi Windows
  Button := TaskDialog.Buttons.Add;
  Button.Caption := 'Poku�aj Ponovo';
  Button.ModalResult := MB_RETRYCANCEL;
  TTaskDialogButtonItem(Button).CommandLinkHint := 'Poku�aj ponovo i ugasi prozor.';
  TaskDialog.OnButtonClicked := TaskDialogButtonClicked;
  // Winapi Windows
  Button := TaskDialog.Buttons.Add;
  Button.Caption := 'Pitaj Google';
  Button.ModalResult := GOOGLE_SEARCH;
  TTaskDialogButtonItem(Button).CommandLinkHint := 'Pretra�i Google i ugasi prozor.';
  TaskDialog.OnButtonClicked := TaskDialogButtonClicked;
end;

procedure TFrmTaskDialogDemo.SetTaskDialogButtonSet(var ButtonSet: TTaskDialogCommonButtons);
begin
  ButtonSet := [];
  if cbBtnOk.Checked then
    Include(ButtonSet, tcbOk);
  if cbBtnYes.Checked then
    Include(ButtonSet, tcbYes);
  if cbBtnNo.Checked then
    Include(ButtonSet, tcbNo);
  if cbBtnCancel.Checked then
    Include(ButtonSet, tcbCancel);
  if cbBtnRetry.Checked then
    Include(ButtonSet, tcbRetry);
  if cbBtnClose.Checked then
    Include(ButtonSet, tcbClose);
end;

procedure TFrmTaskDialogDemo.SetTaskDialogFlags(var FlagsSet: TTaskDialogFlags);
begin
  FlagsSet := [];
  if cbEnableHyperlinks.Checked then
    Include(FlagsSet, tfEnableHyperlinks);

  if cbUseHIconMain.Checked then
    Include(FlagsSet, tfUseHIconMain);

  if cbUseHIconFooter.Checked then
    Include(FlagsSet, tfUseHIconFooter);

  if cbAllowDialogCancellation.Checked then
    FlagsSet := FlagsSet + [tfAllowDialogCancellation];

  if cbUseCommandLinks.Checked then
    Include(FlagsSet, tfUseCommandLinks);

  if cbUseCommandLinksNoIcon.Checked then
    Include(FlagsSet, tfUseCommandLinksNoIcon);

  if cbExpandFooterArea.Checked then
    Include(FlagsSet, tfExpandFooterArea);

  if cbExpandedByDefault.Checked then
    FlagsSet := FlagsSet + [tfExpandedByDefault];

  if cbVerificationFlagChecked.Checked then
    Include(FlagsSet, tfVerificationFlagChecked);

  if cbShowProgressBar.Checked then
    Include(FlagsSet, tfShowProgressBar);

  if cbShowMarqueeProgressBar.Checked then
    Include(FlagsSet, tfShowMarqueeProgressBar);

  if cbCallbackTimer.Checked then
    FlagsSet := FlagsSet + [tfCallbackTimer];

  if cbPositionRelativeToWindow.Checked then
    Include(FlagsSet, tfPositionRelativeToWindow);

  if cbRtlLayout.Checked then
    Include(FlagsSet, tfRtlLayout);

  if cbNoDefaultRadioButton.Checked then
    Include(FlagsSet, tfNoDefaultRadioButton);

  if cbCanBeMinimized .Checked then
    FlagsSet := FlagsSet + [tfCanBeMinimized ];

end;

procedure TFrmTaskDialogDemo.SetComponentDefaultButton(var TaskDialog: TTaskDialog);
begin
  if cbDefaultButton.ItemIndex = -1 then
    TaskDialog.DefaultButton := tcbOk // you can't leave this without value, nil is pointer and not a button
  else
  begin
    if cbDefaultButton.Items[cbDefaultButton.ItemIndex] = 'tcbOk' then
      TaskDialog.DefaultButton := tcbOk;
    if cbDefaultButton.Items[cbDefaultButton.ItemIndex] = 'tcbYes' then
      TaskDialog.DefaultButton := tcbYes;
    if cbDefaultButton.Items[cbDefaultButton.ItemIndex] = 'tcbNo' then
      TaskDialog.DefaultButton := tcbNo;
    if cbDefaultButton.Items[cbDefaultButton.ItemIndex] = 'tcbCancel' then
      TaskDialog.DefaultButton := tcbCancel;
    if cbDefaultButton.Items[cbDefaultButton.ItemIndex] = 'tcbRetry' then
      TaskDialog.DefaultButton := tcbRetry;
    if cbDefaultButton.Items[cbDefaultButton.ItemIndex] = 'tcbClose' then
      TaskDialog.DefaultButton := tcbClose;
  end;
end;

procedure TFrmTaskDialogDemo.SetTaskDialogVerification(var TaskDialog: TTaskDialog);
begin
  TaskDialog.VerificationText := 'Remember my settings.';
  TaskDialog.OnVerificationClicked := TaskDialogVerificationClicked;
end;

procedure TFrmTaskDialogDemo.ExecuteTaskDialog(TaskDialog: TTaskDialog);
begin

  // task dialog will crash the program if running under Windows XP, where there is not task dialog API.
  // It will also not work if visual themes are disabled.
  // In any such case, we need to stick to the old-fashioned MessageBox.
  // Hence, in a real application, we would need to do
  if not ((Win32MajorVersion >= 6) and ThemeServices.ThemesEnabled) then
  begin
    MessageBox(Handle,
             PWideChar(TaskDialog.Caption),
             PWideChar(TaskDialog.Title),
             MB_ICONINFORMATION or MB_OK);
    Exit;
  end;

  // Winapi Windows
  // display the dialog box
  if TaskDialog.Execute = True then
  begin
    memTekstovi.Lines.Insert(0, 'TaskDialog.Execute = True');
  end
  else
  begin
    memTekstovi.Lines.Insert(0, 'TaskDialog.Execute = False');
  end;

  if TaskDialog.RadioButton = nil then
    memTekstovi.Lines.Insert(0, 'TaskDialog.RadioButton not selected.')
  else
    memTekstovi.Lines.Insert(0, 'TaskDialog.RadioButton ' + TaskDialog.RadioButtons.Items[TaskDialog.RadioButton.Index].Caption +
      '  TaskDialog.RadioButton.ID ' + TaskDialog.RadioButton.ID.ToString);

  if TaskDialog.Button = nil then
    memTekstovi.Lines.Insert(0, 'TaskDialog.Button not selected')
  else
    memTekstovi.Lines.Insert(0, 'TaskDialog.Button ' + TaskDialog.Buttons.Items[TaskDialog.Button.Index].Caption);

end;

procedure TFrmTaskDialogDemo.TaskDialog1HyperlinkClicked(Sender: TObject);
begin

  memTekstovi.Lines.Insert(0, 'TaskDialog1 TaskDialog1HyperlinkClicked');
  if Sender is TTaskDialog then
    with Sender as TTaskDialog do
      ShellExecute(0, 'open', PChar(URL), nil, nil, SW_SHOWNORMAL);
end;

procedure TFrmTaskDialogDemo.TaskDialog1Navigated(Sender: TObject);
begin
  memTekstovi.Lines.Insert(0, 'TaskDialog1 TaskDialog1Navigated');
end;

procedure TFrmTaskDialogDemo.TaskDialog1Timer(Sender: TObject;
  TickCount: Cardinal; var Reset: Boolean);
begin
  memTekstovi.Lines.Insert(0, 'TaskDialog1Timer ' + DateTimeToStr(Now));
end;

procedure TFrmTaskDialogDemo.OnRadioButtonClicked(Sender: TObject);
begin
  if (Sender as TTaskDialog).RadioButtons.Items[(Sender as TTaskDialog).RadioButton.Index].Caption = 'Radio 1' then
    ShowMessage('Radio 1 cllicked');
  if (Sender as TTaskDialog).RadioButtons.Items[(Sender as TTaskDialog).RadioButton.Index].Caption = 'Radio 2' then
    ShowMessage('Radio 2 cllicked');
  if (Sender as TTaskDialog).RadioButtons.Items[(Sender as TTaskDialog).RadioButton.Index].Caption = 'Radio 3' then
    ShowMessage('Radio 3 cllicked');
end;

procedure TFrmTaskDialogDemo.btnTaskDialogPositionClick(Sender: TObject);
begin
  memTekstovi.Lines.Insert(0, 'display a task message dialog at a certain position');
  ObradiModalRezultat(TaskMessageDlgPos('Continue', 'Are you sure you want to continue?', mtWarning, mbYesNoCancel,
                      0, Trim(edtX.Text).ToInteger, Trim(edtY.Text).ToInteger));
end;

procedure TFrmTaskDialogDemo.btnTaskMessageDlgClick(Sender: TObject);
begin

  memTekstovi.Lines.Insert(0, 'display another message dialog at the current position');
  ObradiModalRezultat(TaskMessageDlg('Error', 'An error has occured', mtError, mbAbortRetryIgnore, 0));
end;

procedure TFrmTaskDialogDemo.btnZatvoriClick(Sender: TObject);
begin
  Close;
end;

procedure TFrmTaskDialogDemo.btnAPIClick(Sender: TObject);
var
  LTDResult: Integer;
  dwCommonButtons: DWORD;
  pszIcon: LPCWSTR;
begin
  memTekstovi.Lines.Insert(0, 'Dinami�ko kreiranje TTaskDialog objekta u kodu');
  memTekstovi.Lines.Insert(0, 'TaskDialog.ModalResult := MB_OK');

  // The TaskDialog function is declared inside the Winapi.CommCtrl.pas unit.
  case cbIcons.ItemIndex of
    1: pszIcon := TD_WARNING_ICON;
    2: pszIcon := TD_ERROR_ICON;
    3: pszIcon := TD_INFORMATION_ICON;
    4: pszIcon := TD_SHIELD_ICON;
  else
    pszIcon := 0;
  end;

  dwCommonButtons := 0;
  if cbBtnOk.Checked then
    dwCommonButtons := dwCommonButtons or TDCBF_OK_BUTTON;
  if cbBtnYes.Checked then
    dwCommonButtons := dwCommonButtons or TDCBF_YES_BUTTON;
  if cbBtnNo.Checked then
    dwCommonButtons := dwCommonButtons or TDCBF_NO_BUTTON;
  if cbBtnCancel.Checked then
    dwCommonButtons := dwCommonButtons or TDCBF_CANCEL_BUTTON;
  if cbBtnRetry.Checked then
    dwCommonButtons := dwCommonButtons or TDCBF_RETRY_BUTTON;
  if cbBtnClose.Checked then
    dwCommonButtons := dwCommonButtons or TDCBF_CLOSE_BUTTON;

  TaskDialog(0, HInstance,
              PChar(edtTitle.Text),
              PChar(edtCaption.Text),
              PChar(edtText.Text),
              dwCommonButtons,
              pszIcon,
              @LTDResult);
  case LTDResult of
    IDOK:
      begin
        ShowMessage('Clicked OK');
        memTekstovi.Lines.Insert(0, 'TaskDialog LTDResult IDOK');
      end;
    IDCANCEL:
      begin
        ShowMessage('Clicked Cancel');
        memTekstovi.Lines.Insert(0, 'TaskDialog LTDResult IDIDCANCELOK');
      end;
    IDYES:
      begin
        ShowMessage('Clicked OK');
        memTekstovi.Lines.Insert(0, 'TaskDialog LTDResult IDYES');
      end;
    IDNO:
      begin
        ShowMessage('Clicked Cancel');
        memTekstovi.Lines.Insert(0, 'TaskDialog LTDResult IDNO');
      end;
    IDCLOSE:
      begin
        ShowMessage('Clicked OK');
        memTekstovi.Lines.Insert(0, 'TaskDialog LTDResult IDCLOSE');
      end;
    IDRETRY:
      begin
        ShowMessage('Clicked Retry');
        memTekstovi.Lines.Insert(0, 'TaskDialog LTDResult IDRETRY');
      end;
  end;
end;

procedure TFrmTaskDialogDemo.cbBtnCancelClick(Sender: TObject);
begin
  if cbBtnCancel.Checked then
  begin
    if cbDefaultButton.Items.IndexOf('tcbCancel') = -1 then
      cbDefaultButton.Items.Add('tcbCancel');
  end else begin
    if cbDefaultButton.Items.IndexOf('tcbCancel')  > -1 then
    begin
      if cbDefaultButton.Items[cbDefaultButton.ItemIndex] = 'tcbCancel' then
        cbDefaultButton.ItemIndex := -1;
      cbDefaultButton.Items.Delete(cbDefaultButton.Items.IndexOf('tcbCancel'));
    end;
  end;
end;

procedure TFrmTaskDialogDemo.cbBtnCloseClick(Sender: TObject);
begin
  if cbBtnClose.Checked then
  begin
    if cbDefaultButton.Items.IndexOf('tcbClose') = -1 then
      cbDefaultButton.Items.Add('tcbClose');
  end else begin
    if cbDefaultButton.Items.IndexOf('tcbClose')  > -1 then
    begin
      if cbDefaultButton.Items[cbDefaultButton.ItemIndex] = 'tcbClose' then
        cbDefaultButton.ItemIndex := -1;
      cbDefaultButton.Items.Delete(cbDefaultButton.Items.IndexOf('tcbClose'));
    end;
  end;
end;

procedure TFrmTaskDialogDemo.cbBtnNoClick(Sender: TObject);
begin
  if cbBtnNo.Checked then
  begin
    if cbDefaultButton.Items.IndexOf('tcbNo') = -1 then
      cbDefaultButton.Items.Add('tcbNo');
  end else begin
    if cbDefaultButton.Items.IndexOf('tcbNo')  > -1 then
    begin
      if cbDefaultButton.Items[cbDefaultButton.ItemIndex] = 'tcbNo' then
        cbDefaultButton.ItemIndex := -1;
      cbDefaultButton.Items.Delete(cbDefaultButton.Items.IndexOf('tcbNo'));
    end;
  end;
end;

procedure TFrmTaskDialogDemo.cbBtnOkClick(Sender: TObject);
begin
  // Default button uvijek mora imati vrijednost i ona neka bude tcbOk
  ;
//  if cbBtnOk.Checked then
//  begin
//    if cbDefaultButton.Items.IndexOf('tcbOk') = -1 then
//      cbDefaultButton.Items.Add('tcbOk');
//  end else begin
//    if cbDefaultButton.Items.IndexOf('tcbOk')  > -1 then
//    begin
//      if cbDefaultButton.Items[cbDefaultButton.ItemIndex] = 'tcbOk' then
//        cbDefaultButton.ItemIndex := -1;
//      cbDefaultButton.Items.Delete(cbDefaultButton.Items.IndexOf('tcbOk'));
//    end;
//  end;

end;

procedure TFrmTaskDialogDemo.cbBtnRetryClick(Sender: TObject);
begin
  if cbBtnRetry.Checked then
  begin
    if cbDefaultButton.Items.IndexOf('tcbRetry') = -1 then
      cbDefaultButton.Items.Add('tcbRetry');
  end else begin
    if cbDefaultButton.Items.IndexOf('tcbRetry')  > -1 then
    begin
      if cbDefaultButton.Items[cbDefaultButton.ItemIndex] = 'tcbRetry' then
        cbDefaultButton.ItemIndex := -1;
      cbDefaultButton.Items.Delete(cbDefaultButton.Items.IndexOf('tcbRetry'));
    end;
  end;
end;

procedure TFrmTaskDialogDemo.cbBtnYesClick(Sender: TObject);
begin
  if cbBtnYes.Checked then
  begin
    if cbDefaultButton.Items.IndexOf('tcbYes') = -1 then
      cbDefaultButton.Items.Add('tcbYes');
  end else begin
    if cbDefaultButton.Items.IndexOf('tcbYes')  > -1 then
    begin
      if cbDefaultButton.Items[cbDefaultButton.ItemIndex] = 'tcbYes' then
        cbDefaultButton.ItemIndex := -1;
      cbDefaultButton.Items.Delete(cbDefaultButton.Items.IndexOf('tcbYes'));
    end;
  end;
end;

procedure TFrmTaskDialogDemo.FormShow(Sender: TObject);
begin
  cbIcons.ItemIndex := 4;
end;

function TFrmTaskDialogDemo.IsPrimeNumber(Number: Integer): Boolean;
var
  M: Integer;
begin
  Assert(Number > 0);
  if Number = 1 then // annoying special case
  begin
    Result := False;
    exit;
  end;
  for M := 2 to (Number div 2) do
  begin
    if Number mod M = 0 then
    begin
      Result := False;
      exit;
    end;
  end;
  Result := True;
end;

procedure TFrmTaskDialogDemo.btnKomponentaBotuniClick(Sender: TObject);
begin
  OsnovniTaskDialog(TaskDialog1);
  AddButtons(TaskDialog1);
  ExecuteTaskDialog(TaskDialog1);
  ObradiModalRezultat(TaskDialog1.ModalResult);
end;

procedure TFrmTaskDialogDemo.TaskDialogButtonClicked(Sender: TObject;
  ModalResult: TModalResult; var CanClose: Boolean);
begin
  if TTaskDialog(Sender).Button = nil then Exit;

  if TTaskDialog(Sender).Button.Index = 0 then
  begin
    memTekstovi.Lines.Insert(0, (TTaskDialog(Sender).Button.Caption + ' button clicked.'));
    TTaskDialog(Sender).ProgressBar.Position := TTaskDialog(Sender).ProgressBar.Position + 10;
    CanClose := (TTaskDialog(Sender).ProgressBar.Position >= TTaskDialog(Sender).ProgressBar.Max);
  end;
  if TTaskDialog(Sender).Button.Index = 1 then
  begin
    ShowMessage(TTaskDialog(Sender).Button.Caption + ' button clicked.');
    CanClose := True;
  end;
  if TTaskDialog(Sender).Button.Index = 2 then
  begin
    CanClose := True;
  end;
end;

procedure TFrmTaskDialogDemo.TaskDialogVerificationClicked(Sender: TObject);
begin
   if tfVerificationFlagChecked in TTaskDialog(Sender).Flags then
      ShowMessage('checked')
   else
      ShowMessage('not checked');
end;

procedure TFrmTaskDialogDemo.tdProgressButtonClicked(Sender: TObject;
  ModalResult: TModalResult; var CanClose: Boolean);
begin
  if not FFinished then
  begin
    TaskDialog1.OnTimer := nil;
    ShowMessage('Calculation aborted by user');
    CanClose := True;
  end;
end;

procedure TFrmTaskDialogDemo.btnProgressClick(Sender: TObject);
var
  Button: TTaskDialogBaseButtonItem;
begin
  FCurrNumber := 1;
  FFinished := False;
  FPrimeNumbersCount := 0;

  TaskDialog1.Caption := 'Prosti Brojevi';
  TaskDialog1.Title := 'Koliko ima prostih brojeva od 1 do 1000?';
  TaskDialog1.Text := 'Na timer okini funkciju i kreni ra�unati';

  TaskDialog1.Buttons.Clear;
  Button := TaskDialog1.Buttons.Add;
  Button.Caption := 'Korak naprijed';
  TTaskDialogButtonItem(Button).CommandLinkHint := 'I noga u guzicu je korak naprijed.';
  TaskDialog1.OnButtonClicked := tdProgressButtonClicked;

  TaskDialog1.Flags := [tfShowProgressBar] + [tfCallbackTimer];
  TaskDialog1.ProgressBar.Min := 0;
  TaskDialog1.ProgressBar.Max := 100;
  TaskDialog1.ProgressBar.Position := 0;
  TaskDialog1.OnTimer := tdProgressTimer;
  TaskDialog1.Execute;
end;

procedure TFrmTaskDialogDemo.tdProgressTimer(Sender: TObject; TickCount: Cardinal;
  var Reset: Boolean);
var
  I: Integer;
begin
  for I := 1 to NUMBERS_IN_A_SINGLE_STEP do
  begin
    if IsPrimeNumber(FCurrNumber) then
      Inc(FPrimeNumbersCount);
    TaskDialog1.ProgressBar.Position := FCurrNumber * 100 div MAX_NUMBERS;
    Inc(FCurrNumber);
  end;

  FFinished := FCurrNumber >= MAX_NUMBERS;
  if FFinished then
  begin
    TaskDialog1.OnTimer := nil;
    TaskDialog1.ProgressBar.Position := 100;
    ShowMessage('There are ' + FPrimeNumbersCount.ToString + ' prime numbers up to ' + MAX_NUMBERS.ToString);
  end;
end;

end.
