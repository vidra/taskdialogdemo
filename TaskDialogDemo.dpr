program TaskDialogDemo;

uses
  Vcl.Forms,
  uFrmTaskDialogDemo in 'uFrmTaskDialogDemo.pas' {FrmTaskDialogDemo},
  uAnonProcToNotify in 'uAnonProcToNotify.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFrmTaskDialogDemo, FrmTaskDialogDemo);
  Application.Run;
end.
