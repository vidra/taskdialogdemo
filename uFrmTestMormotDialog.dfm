object FrmTestMormotDialog: TFrmTestMormotDialog
  Left = 0
  Top = 0
  Caption = 'SynTaskDialog Demo'
  ClientHeight = 602
  ClientWidth = 912
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  DesignSize = (
    912
    602)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 168
    Top = 21
    Width = 20
    Height = 13
    Caption = 'Title'
  end
  object Label2: TLabel
    Left = 168
    Top = 52
    Width = 37
    Height = 13
    Caption = 'Caption'
  end
  object Label5: TLabel
    Left = 168
    Top = 83
    Width = 26
    Height = 13
    Caption = 'Tekst'
  end
  object Label8: TLabel
    Left = 168
    Top = 114
    Width = 61
    Height = 13
    Caption = 'Footer Tekst'
  end
  object pblControls: TPanel
    Left = 0
    Top = 297
    Width = 912
    Height = 41
    Align = alBottom
    TabOrder = 0
    DesignSize = (
      912
      41)
    object btnZatvori: TButton
      Left = 824
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = 'Zatvori'
      TabOrder = 0
      OnClick = btnZatvoriClick
    end
    object btnObrisiTekst: TButton
      Left = 743
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = 'Obri'#353'i tekst'
      TabOrder = 1
      OnClick = btnObrisiTekstClick
    end
    object btnQuestion: TButton
      Left = 656
      Top = 8
      Width = 81
      Height = 25
      Caption = 'Question'
      TabOrder = 2
      OnClick = btnQuestionClick
    end
    object btnInformation: TButton
      Left = 569
      Top = 8
      Width = 81
      Height = 25
      Caption = 'Information'
      TabOrder = 3
      OnClick = btnInformationClick
    end
  end
  object memTekstovi: TMemo
    Left = 0
    Top = 338
    Width = 912
    Height = 264
    Align = alBottom
    ScrollBars = ssBoth
    TabOrder = 1
  end
  object edtTitle: TEdit
    Left = 248
    Top = 18
    Width = 544
    Height = 21
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 2
    Text = 'Title'
  end
  object edtCaption: TEdit
    Left = 248
    Top = 49
    Width = 544
    Height = 21
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 3
    Text = 'Caption'
  end
  object edtText: TEdit
    Left = 248
    Top = 80
    Width = 544
    Height = 21
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 4
    Text = 
      'Tekst poruke za task dialog <A href="https://www.nba.com">Link</' +
      'A>'
  end
  object edtFooterText: TEdit
    Left = 248
    Top = 111
    Width = 544
    Height = 21
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 5
    Text = 
      'Emulation code will handle only tdfUseCommandLinks, tdfUseComman' +
      'dLinksNoIcon, and tdfQuery options'
  end
  object btnSynTask: TButton
    Left = 16
    Top = 16
    Width = 89
    Height = 25
    Caption = 'SynTask'
    TabOrder = 6
    OnClick = btnSynTaskClick
  end
  object gbFlags: TGroupBox
    Left = 168
    Top = 133
    Width = 624
    Height = 124
    ParentCustomHint = False
    Caption = 'Flags'
    ParentShowHint = False
    ShowHint = False
    TabOrder = 7
    object cbEnableHyperlinks: TCheckBox
      Left = 15
      Top = 20
      Width = 105
      Height = 17
      Hint = 
        'If set, content, footer, and expanded text can include hyperlink' +
        's. '
      Caption = 'tfEnableHyperlinks '
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
    end
    object cbUseHIconMain: TCheckBox
      Left = 167
      Top = 20
      Width = 97
      Height = 17
      Hint = 'If set, use the custom main icon. '
      Caption = 'tfUseHiconMain '
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
    end
    object cbUseHIconFooter: TCheckBox
      Left = 324
      Top = 20
      Width = 108
      Height = 17
      Hint = 'If set, use the custom footer icon. '
      Caption = 'tfUseHiconFooter'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
    end
    object cbAllowDialogCancellation: TCheckBox
      Left = 476
      Top = 20
      Width = 145
      Height = 17
      Hint = 
        'If set, permits Task Dialog to be closed in the absence of a Can' +
        'cel button.'
      Caption = 'tfAllowDialogCancellation'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
    end
    object cbCanBeMinimized: TCheckBox
      Left = 476
      Top = 89
      Width = 117
      Height = 17
      Hint = 'If set, the Task Dialog can be minimized. '
      Caption = 'tfCanBeMinimized '
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
    end
    object cbNoDefaultRadioButton: TCheckBox
      Left = 325
      Top = 89
      Width = 145
      Height = 17
      Hint = 'If set, there is no default radio button. '
      Caption = 'tfNoDefaultRadioButton'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 5
    end
    object cbPositionRelativeToWindow: TCheckBox
      Left = 15
      Top = 89
      Width = 150
      Height = 17
      Hint = 'If set, Task Dialog is centered with respect to parent window.'
      Caption = 'tfPositionRelativeToWindow'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 6
    end
    object cbShowProgressBar: TCheckBox
      Left = 167
      Top = 66
      Width = 123
      Height = 17
      Hint = 'If set, display the progress bar. '
      Caption = 'tfShowProgressBar'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 7
    end
    object cbCallbackTimer: TCheckBox
      Left = 476
      Top = 66
      Width = 97
      Height = 17
      Hint = 'If set, dialogs callback will be called every 200 milliseconds. '
      Caption = 'tfCallbackTimer'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 8
    end
    object cbShowMarqueeProgressBar: TCheckBox
      Left = 324
      Top = 66
      Width = 152
      Height = 17
      Hint = 'If set, display the marquee progress bar.'
      Caption = 'tfShowMarqueeProgressBar'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 9
    end
    object cbVerificationFlagChecked: TCheckBox
      Left = 15
      Top = 66
      Width = 137
      Height = 17
      Hint = 'If set, the verification check box is initially checked. '
      Caption = 'tfVerificationFlagChecked '
      ParentShowHint = False
      ShowHint = True
      TabOrder = 10
    end
    object cbUseCommandLinks: TCheckBox
      Left = 15
      Top = 43
      Width = 122
      Height = 17
      Hint = 
        'If set, buttons are displayed as command links using a standard ' +
        'dialog glyph. '
      Caption = 'tfUseCommandLinks '
      ParentShowHint = False
      ShowHint = True
      TabOrder = 11
    end
    object cbUseCommandLinksNoIcon: TCheckBox
      Left = 167
      Top = 43
      Width = 151
      Height = 17
      Hint = 'If set, buttons are displayed as command links without a glyph. '
      Caption = 'tfUseCommandLinksNoIcon'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 12
    end
    object cbExpandFooterArea: TCheckBox
      Left = 324
      Top = 43
      Width = 124
      Height = 17
      Hint = 'If set, display expanded text in the footer. '
      Caption = 'tfExpandFooterArea'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 13
    end
    object cbExpandedByDefault: TCheckBox
      Left = 476
      Top = 43
      Width = 129
      Height = 17
      Hint = 'If set, expanded text is displayed when the Task Dialog opens.'
      Caption = 'tfExpandedByDefault'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 14
    end
    object cbRtlLayout: TCheckBox
      Left = 167
      Top = 89
      Width = 113
      Height = 17
      Hint = 'If set, text reads right-to-left. '
      Caption = 'tfRtlLayout'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 15
    end
  end
  object cbRadioButtons: TCheckBox
    Left = 17
    Top = 47
    Width = 88
    Height = 17
    Caption = 'Radio Buttons'
    TabOrder = 8
  end
  object cbVerification: TCheckBox
    Left = 17
    Top = 70
    Width = 88
    Height = 17
    Caption = 'Verification'
    TabOrder = 9
  end
  object Button1: TButton
    Left = 17
    Top = 125
    Width = 88
    Height = 25
    Caption = 'ShowMessage'
    TabOrder = 10
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 17
    Top = 156
    Width = 88
    Height = 25
    Caption = 'InputQuery'
    TabOrder = 11
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 17
    Top = 187
    Width = 88
    Height = 25
    Caption = 'InputSelect'
    TabOrder = 12
    OnClick = Button3Click
  end
  object btnSynTaskEx: TButton
    Left = 17
    Top = 218
    Width = 89
    Height = 25
    Caption = 'SynTaskEx'
    TabOrder = 13
    OnClick = btnSynTaskExClick
  end
  object cbNoviBotuni: TCheckBox
    Left = 17
    Top = 93
    Width = 97
    Height = 17
    Caption = 'Novi Botuni'
    TabOrder = 14
  end
end
