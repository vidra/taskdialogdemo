unit untFrmTaskDialog;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TFrmTaskDialog = class(TForm)
    Button1: TButton;
    Button2: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure TaskDialogButtonClicked(Sender: TObject;
                                      ModalResult: TModalResult;
                                      var CanClose: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmTaskDialog: TFrmTaskDialog;

implementation

{$R *.dfm}

uses Vcl.ComCtrls;

procedure TFrmTaskDialog.Button1Click(Sender: TObject);
begin
  Close;
end;

procedure TFrmTaskDialog.Button2Click(Sender: TObject);
var
  TaskDialog: TTaskDialog;
  Button: TTaskDialogBaseButtonItem;
begin
//  // display a task message dialog at a certain position
//  TaskMessageDlgPos('Continue', 'Are you sure you want to continue?', mtWarning, mbYesNoCancel, 0, 10, 10);
//
//  // display another message dialog at the current position
//  TaskMessageDlg('Error', 'An error has occured', mtError, mbAbortRetryIgnore, 0);

  // create a custom task dialog
  TaskDialog := TTaskDialog.Create(Self);
  try
    TaskDialog.Title := 'Restarting services';
    TaskDialog.Caption := 'Status';
    TaskDialog.Text := 'Apply all pressed';

    // assign a MB_OK modal result to the task dialog
    TaskDialog.ModalResult := MB_OK;

    // add some customized buttons to the task dialog
    with TaskDialog.Buttons.Add do
    begin
      Caption := 'Demo';
      ModalResult := mrNone;
    end;

    TaskDialog.OnButtonClicked := TaskDialogButtonClicked;
    TaskDialog.MainIcon := tdiInformation;
    TaskDialog.ProgressBar.Position := 40;
    TaskDialog.ProgressBar.MarqueeSpeed := 1;
    TaskDialog.ExpandButtonCaption := 'Current status';
    TaskDialog.ExpandedText := 'Status is 40';
    TaskDialog.Flags := [tfShowProgressBar, tfExpandedByDefault];
    TaskDialog.Execute;
  finally
    TaskDialog.Free;
  end;

end;

procedure TFrmTaskDialog.TaskDialogButtonClicked(Sender: TObject;
  ModalResult: TModalResult; var CanClose: Boolean);
begin
  if TTaskDialog(Sender).Button = nil then
    Exit;

  if TTaskDialog(Sender).Button.Index = 0 then
  begin
    if TTaskDialog(Sender).ProgressBar.Position < 100 then
    begin
      CanClose := False;
      TTaskDialog(Sender).ProgressBar.Position := TTaskDialog(Sender).ProgressBar.Position + 10;
      TTaskDialog(Sender).Text := 'Next service step';
      TTaskDialog(Sender).ExpandedText := 'Status is ' + TTaskDialog(Sender).ProgressBar.Position.ToString;
    end else begin
      CanClose := True;
    end;
  end else begin
      CanClose := True;
  end;
end;

end.
