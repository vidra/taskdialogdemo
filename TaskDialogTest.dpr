program TaskDialogTest;

uses
  Vcl.Forms,
  untFrmTaskDialog in 'untFrmTaskDialog.pas' {FrmTaskDialog};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFrmTaskDialog, FrmTaskDialog);
  Application.Run;
end.
