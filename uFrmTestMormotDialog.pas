unit uFrmTestMormotDialog;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls;

type
  TFrmTestMormotDialog = class(TForm)
    btnInformation: TButton;
    btnQuestion: TButton;
    pblControls: TPanel;
    btnZatvori: TButton;
    btnObrisiTekst: TButton;
    memTekstovi: TMemo;
    Label1: TLabel;
    edtTitle: TEdit;
    Label2: TLabel;
    edtCaption: TEdit;
    Label5: TLabel;
    edtText: TEdit;
    Label8: TLabel;
    edtFooterText: TEdit;
    btnSynTask: TButton;
    gbFlags: TGroupBox;
    cbEnableHyperlinks: TCheckBox;
    cbUseHIconMain: TCheckBox;
    cbUseHIconFooter: TCheckBox;
    cbAllowDialogCancellation: TCheckBox;
    cbCanBeMinimized: TCheckBox;
    cbNoDefaultRadioButton: TCheckBox;
    cbPositionRelativeToWindow: TCheckBox;
    cbShowProgressBar: TCheckBox;
    cbCallbackTimer: TCheckBox;
    cbShowMarqueeProgressBar: TCheckBox;
    cbVerificationFlagChecked: TCheckBox;
    cbUseCommandLinks: TCheckBox;
    cbUseCommandLinksNoIcon: TCheckBox;
    cbExpandFooterArea: TCheckBox;
    cbExpandedByDefault: TCheckBox;
    cbRtlLayout: TCheckBox;
    cbRadioButtons: TCheckBox;
    cbVerification: TCheckBox;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    btnSynTaskEx: TButton;
    cbNoviBotuni: TCheckBox;
    procedure btnInformationClick(Sender: TObject);
    procedure btnQuestionClick(Sender: TObject);
    procedure btnObrisiTekstClick(Sender: TObject);
    procedure btnZatvoriClick(Sender: TObject);
    procedure btnSynTaskClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure btnSynTaskExClick(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
  end;

var
  FrmTestMormotDialog: TFrmTestMormotDialog;

implementation

uses SynTaskDialog
  , ShellApi
  , uZamjenaSynTask;

{$R *.dfm}

procedure TFrmTestMormotDialog.btnObrisiTekstClick(Sender: TObject);
begin
  memTekstovi.Lines.Clear;
end;

procedure TFrmTestMormotDialog.btnQuestionClick(Sender: TObject);
var
  Task: TTaskDialog;
  UserAnswer: Integer;
begin
  Task.Inst := 'Question';
  Task.Content := 'What do you want to do?';
  Task.Footer := 'Please choose your answer.';
  Task.Buttons := 'Close'+'\n'+'Terminates application' + sLineBreak + 'Dismiss'+'\n'+'Nothing happens';
  UserAnswer:= Task.Execute([], 101, [tdfUseCommandLinks,tdfAllowDialogCancellation], tiQuestion, tfiInformation, 0, 0, Handle, False);
  if UserAnswer=100 then Application.Terminate;
end;

procedure TFrmTestMormotDialog.btnSynTaskClick(Sender: TObject);
var
  TaskDialog: TTaskDialog;
  FlagsSet: TTaskDialogFlags;
begin
  TaskDialog.Inst := edtCaption.Text;
  TaskDialog.Content := edtText.Text;
  TaskDialog.Title := edtTitle.Text;
  TaskDialog.Footer := edtFooterText.Text;
  FlagsSet := [];
  if cbEnableHyperlinks.Checked then
  begin
    Include(FlagsSet, tdfEnableHyperlinks);

  end;

  if cbUseHIconMain.Checked then
    Include(FlagsSet, tdfUseHIconMain);

  if cbUseHIconFooter.Checked then
    Include(FlagsSet, tdfUseHIconFooter);

  if cbAllowDialogCancellation.Checked then
    FlagsSet := FlagsSet + [tdfAllowDialogCancellation];

  if cbUseCommandLinks.Checked then
    Include(FlagsSet, tdfUseCommandLinks);

  if cbUseCommandLinksNoIcon.Checked then
    Include(FlagsSet, tdfUseCommandLinksNoIcon);

  if cbExpandFooterArea.Checked then
    Include(FlagsSet, tdfExpandFooterArea);

  if cbExpandedByDefault.Checked then
    FlagsSet := FlagsSet + [tdfExpandByDefault];

  if cbVerificationFlagChecked.Checked then
    Include(FlagsSet, tdfVerificationFlagChecked);

  if cbShowProgressBar.Checked then
    Include(FlagsSet, tdfShowProgressBar);

  if cbShowMarqueeProgressBar.Checked then
    Include(FlagsSet, tdfShowMarqueeProgressBar);

  if cbCallbackTimer.Checked then
    FlagsSet := FlagsSet + [tdfCallbackTimer];

  if cbPositionRelativeToWindow.Checked then
    Include(FlagsSet, tdfPositionRelativeToWindow);

  if cbRtlLayout.Checked then
    Include(FlagsSet, tdfRtlLayout);

  if cbNoDefaultRadioButton.Checked then
    Include(FlagsSet, tdfNoDefaultRadioButton);

  if cbCanBeMinimized.Checked then
    FlagsSet := FlagsSet + [tdfCanBeMinimized ];
  TaskDialog.Radios := '';
  if cbRadioButtons.Checked then
  begin
    TaskDialog.Radios := 'Radio 1'#10'Radio 2'#10'Radio 3';
  end;
  TaskDialog.Verify := '';
  if cbVerification.Checked then
  begin
    TaskDialog.Verify := 'Do no ask for this setting next time';
    TaskDialog.VerifyChecked := false;
  end;
  TaskDialog.Buttons := '';
  if cbNoviBotuni.Checked then
    TaskDialog.Buttons := 'Button 1'#13#10'Button 2'#13#10'Button 3';

  TaskDialog.Execute([cbOK], mrOk, FlagsSet, tiInformation);

  memTekstovi.Lines.Insert(0, 'TaskDialog.RadioRes = ' + TaskDialog.RadioRes.ToString);
  memTekstovi.Lines.Insert(0, 'TaskDialog.VerifyChecked = ' + TaskDialog.VerifyChecked.ToString);
  memTekstovi.Lines.Insert(0, 'TaskDialog.SelectionRes = ' + TaskDialog.SelectionRes.ToString);
end;

procedure TFrmTestMormotDialog.btnSynTaskExClick(Sender: TObject);
var
  TaskEx: TTaskDialogEx;
  MyResult: Integer;
  FlagsSet: TTaskDialogFlags;
begin
  TaskEx.Init;
  TaskEx.Base.Title := edtTitle.Text;
  TaskEx.Base.Inst := edtCaption.Text;

FlagsSet := [];
  if cbEnableHyperlinks.Checked then
  begin
    Include(FlagsSet, tdfEnableHyperlinks);

  end;

  if cbUseHIconMain.Checked then
    Include(FlagsSet, tdfUseHIconMain);

  if cbUseHIconFooter.Checked then
    Include(FlagsSet, tdfUseHIconFooter);

  if cbAllowDialogCancellation.Checked then
    FlagsSet := FlagsSet + [tdfAllowDialogCancellation];

  if cbUseCommandLinks.Checked then
    Include(FlagsSet, tdfUseCommandLinks);

  if cbUseCommandLinksNoIcon.Checked then
    Include(FlagsSet, tdfUseCommandLinksNoIcon);

  if cbExpandFooterArea.Checked then
    Include(FlagsSet, tdfExpandFooterArea);

  if cbExpandedByDefault.Checked then
    FlagsSet := FlagsSet + [tdfExpandByDefault];

  if cbVerificationFlagChecked.Checked then
    Include(FlagsSet, tdfVerificationFlagChecked);

  if cbShowProgressBar.Checked then
    Include(FlagsSet, tdfShowProgressBar);

  if cbShowMarqueeProgressBar.Checked then
    Include(FlagsSet, tdfShowMarqueeProgressBar);

  if cbCallbackTimer.Checked then
    FlagsSet := FlagsSet + [tdfCallbackTimer];

  if cbPositionRelativeToWindow.Checked then
    Include(FlagsSet, tdfPositionRelativeToWindow);

  if cbRtlLayout.Checked then
    Include(FlagsSet, tdfRtlLayout);

  if cbNoDefaultRadioButton.Checked then
    Include(FlagsSet, tdfNoDefaultRadioButton);

  if cbCanBeMinimized.Checked then
    FlagsSet := FlagsSet + [tdfCanBeMinimized ];
  TaskEx.Flags := FlagsSet;
  MyResult := TaskEx.Execute;
  memTekstovi.Lines.Insert(0, 'TaskEx.Execute = ' + MyResult.ToString);
end;

procedure TFrmTestMormotDialog.btnZatvoriClick(Sender: TObject);
begin
  Close;
end;

procedure TFrmTestMormotDialog.Button1Click(Sender: TObject);
begin
  ShowMessage(edtText.Text, edtCaption.Text, False);
  ShowMessage(edtText.Text, edtCaption.Text, True);
end;

procedure TFrmTestMormotDialog.Button2Click(Sender: TObject);
var MyValue: string;
begin
  if InputQuery(edtCaption.Text, edtText.Text, MyValue) then
    memTekstovi.Lines.Insert(0, 'True InputQuery(edtCaption.Text, edtText.Text, MyValue) = ' + MyValue)
  else
    memTekstovi.Lines.Insert(0, 'False InputQuery(edtCaption.Text, edtText.Text, MyValue) = ' + MyValue)
end;

procedure TFrmTestMormotDialog.Button3Click(Sender: TObject);
var
  MyResult: Integer;
begin
  MyResult := InputSelect(edtCaption.Text, edtText.Text, 'opcija 1'#13#10'opcija 2'#13#10'opcija 3'#13#10'opcija 4', edtFooterText.Text);
  memTekstovi.Lines.Insert(0, 'InputSelect(edtCaption.Text, edtText.Text, AItemsText, ASelectedText: string) = ' + MyResult.ToString);
end;

procedure TFrmTestMormotDialog.btnInformationClick(Sender: TObject);
var
  Task: TTaskDialog;
begin
  Task.Inst := 'Information';
  Task.Content := 'File includes non-unicode characters.';
  Task.Footer := 'Displaying as ANSI instead of UTF-8.';
  Task.Execute([cbOK],mrOk,[],tiInformation);
end;



end.
