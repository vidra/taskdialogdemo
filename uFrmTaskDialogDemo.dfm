object FrmTaskDialogDemo: TFrmTaskDialogDemo
  Left = 0
  Top = 0
  Caption = 'Task Dialog Demo'
  ClientHeight = 696
  ClientWidth = 805
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  DesignSize = (
    805
    696)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 168
    Top = 21
    Width = 20
    Height = 13
    Caption = 'Title'
  end
  object Label2: TLabel
    Left = 168
    Top = 52
    Width = 37
    Height = 13
    Caption = 'Caption'
  end
  object Label3: TLabel
    Left = 592
    Top = 115
    Width = 10
    Height = 13
    Anchors = [akTop, akRight]
    Caption = 'X:'
  end
  object Label4: TLabel
    Left = 695
    Top = 114
    Width = 10
    Height = 13
    Anchors = [akTop, akRight]
    Caption = 'Y:'
  end
  object Label5: TLabel
    Left = 168
    Top = 83
    Width = 26
    Height = 13
    Caption = 'Tekst'
  end
  object Label6: TLabel
    Left = 168
    Top = 314
    Width = 21
    Height = 13
    Caption = 'Icon'
  end
  object Label7: TLabel
    Left = 569
    Top = 314
    Width = 70
    Height = 13
    Caption = 'Default Button'
  end
  object Label8: TLabel
    Left = 168
    Top = 341
    Width = 61
    Height = 13
    Caption = 'Footer Tekst'
  end
  object Label9: TLabel
    Left = 354
    Top = 314
    Width = 56
    Height = 13
    Caption = 'Footer Icon'
  end
  object Label10: TLabel
    Left = 349
    Top = 116
    Width = 42
    Height = 13
    Caption = 'Marquee'
  end
  object Label11: TLabel
    Left = 456
    Top = 116
    Width = 21
    Height = 13
    Caption = '[ms]'
  end
  object Label12: TLabel
    Left = 569
    Top = 369
    Width = 69
    Height = 13
    Caption = 'Progress Type'
  end
  object Label13: TLabel
    Left = 450
    Top = 369
    Width = 37
    Height = 13
    Caption = 'Position'
  end
  object Label14: TLabel
    Left = 168
    Top = 369
    Width = 61
    Height = 13
    Caption = 'Progress Min'
  end
  object Label15: TLabel
    Left = 302
    Top = 369
    Width = 65
    Height = 13
    Caption = 'Progress Max'
  end
  object pblControls: TPanel
    Left = 0
    Top = 655
    Width = 805
    Height = 41
    Align = alBottom
    TabOrder = 0
    DesignSize = (
      805
      41)
    object btnZatvori: TButton
      Left = 717
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = 'Zatvori'
      TabOrder = 0
      OnClick = btnZatvoriClick
    end
    object btnObrisiTekst: TButton
      Left = 636
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = 'Obri'#353'i tekst'
      TabOrder = 1
      OnClick = btnObrisiTekstClick
    end
  end
  object btnBasicDemo: TButton
    Left = 16
    Top = 16
    Width = 97
    Height = 25
    Caption = 'Basic Demo'
    TabOrder = 1
    OnClick = btnBasicDemoClick
  end
  object btnTaskMessageDlg: TButton
    Left = 16
    Top = 47
    Width = 97
    Height = 25
    Caption = 'TaskMessageDlg'
    TabOrder = 2
    OnClick = btnTaskMessageDlgClick
  end
  object btnTaskDialogPosition: TButton
    Left = 16
    Top = 78
    Width = 97
    Height = 25
    Caption = 'Position'
    TabOrder = 3
    OnClick = btnTaskDialogPositionClick
  end
  object btnKomponenta: TButton
    Left = 16
    Top = 109
    Width = 97
    Height = 25
    Caption = 'Komponenta'
    TabOrder = 4
    OnClick = btnKomponentaClick
  end
  object memTekstovi: TMemo
    Left = 0
    Top = 391
    Width = 805
    Height = 264
    Align = alBottom
    ScrollBars = ssBoth
    TabOrder = 5
  end
  object edtTitle: TEdit
    Left = 248
    Top = 18
    Width = 544
    Height = 21
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 6
    Text = 'Title'
  end
  object edtCaption: TEdit
    Left = 248
    Top = 49
    Width = 544
    Height = 21
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 7
    Text = 'Caption'
  end
  object edtX: TEdit
    Left = 616
    Top = 111
    Width = 50
    Height = 21
    Anchors = [akTop, akRight]
    NumbersOnly = True
    TabOrder = 8
    Text = '10'
  end
  object edtY: TEdit
    Left = 719
    Top = 111
    Width = 50
    Height = 21
    Anchors = [akTop, akRight]
    NumbersOnly = True
    TabOrder = 9
    Text = '10'
  end
  object edtText: TEdit
    Left = 248
    Top = 80
    Width = 544
    Height = 21
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 10
    Text = 
      'Tekst poruke za task dialog <A href="https://www.nba.com">Link</' +
      'A>'
  end
  object btnDemoButtons: TButton
    Left = 16
    Top = 140
    Width = 97
    Height = 25
    Caption = 'Novi Botuni'
    TabOrder = 11
    OnClick = btnDemoButtonsClick
  end
  object btnKomponentaBotuni: TButton
    Left = 16
    Top = 169
    Width = 97
    Height = 25
    Caption = 'Kompon + Botuni'
    TabOrder = 12
    OnClick = btnKomponentaBotuniClick
  end
  object gbButtons: TGroupBox
    Left = 168
    Top = 138
    Width = 624
    Height = 37
    Caption = 'Buttons'
    TabOrder = 13
    object cbBtnCancel: TCheckBox
      Left = 325
      Top = 15
      Width = 97
      Height = 17
      Caption = 'tcbCancel'
      TabOrder = 0
      OnClick = cbBtnCancelClick
    end
    object cbBtnClose: TCheckBox
      Left = 424
      Top = 15
      Width = 97
      Height = 17
      Caption = 'tcbClose'
      TabOrder = 1
      OnClick = cbBtnCloseClick
    end
    object cbBtnNo: TCheckBox
      Left = 222
      Top = 15
      Width = 97
      Height = 17
      Caption = 'tcbNo'
      TabOrder = 2
      OnClick = cbBtnNoClick
    end
    object cbBtnOk: TCheckBox
      Left = 16
      Top = 15
      Width = 97
      Height = 17
      Caption = 'tcbOk'
      Checked = True
      State = cbChecked
      TabOrder = 3
      OnClick = cbBtnOkClick
    end
    object cbBtnRetry: TCheckBox
      Left = 527
      Top = 15
      Width = 97
      Height = 17
      Caption = 'tcbRetry'
      TabOrder = 4
      OnClick = cbBtnRetryClick
    end
    object cbBtnYes: TCheckBox
      Left = 119
      Top = 15
      Width = 97
      Height = 17
      Caption = 'tcbYes'
      TabOrder = 5
      OnClick = cbBtnYesClick
    end
  end
  object cbExtraText: TCheckBox
    Left = 492
    Top = 113
    Width = 97
    Height = 17
    Anchors = [akTop, akRight]
    Caption = 'Extra Text'
    TabOrder = 14
  end
  object gbFlags: TGroupBox
    Left = 168
    Top = 181
    Width = 624
    Height = 124
    ParentCustomHint = False
    Caption = 'Flags'
    ParentShowHint = False
    ShowHint = False
    TabOrder = 15
    object cbEnableHyperlinks: TCheckBox
      Left = 15
      Top = 20
      Width = 105
      Height = 17
      Hint = 
        'If set, content, footer, and expanded text can include hyperlink' +
        's. '
      Caption = 'tfEnableHyperlinks '
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
    end
    object cbUseHIconMain: TCheckBox
      Left = 167
      Top = 20
      Width = 97
      Height = 17
      Hint = 'If set, use the custom main icon. '
      Caption = 'tfUseHiconMain '
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
    end
    object cbUseHIconFooter: TCheckBox
      Left = 324
      Top = 20
      Width = 108
      Height = 17
      Hint = 'If set, use the custom footer icon. '
      Caption = 'tfUseHiconFooter'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
    end
    object cbAllowDialogCancellation: TCheckBox
      Left = 476
      Top = 20
      Width = 145
      Height = 17
      Hint = 
        'If set, permits Task Dialog to be closed in the absence of a Can' +
        'cel button.'
      Caption = 'tfAllowDialogCancellation'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
    end
    object cbCanBeMinimized: TCheckBox
      Left = 476
      Top = 89
      Width = 117
      Height = 17
      Hint = 'If set, the Task Dialog can be minimized. '
      Caption = 'tfCanBeMinimized '
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
    end
    object cbNoDefaultRadioButton: TCheckBox
      Left = 325
      Top = 89
      Width = 145
      Height = 17
      Hint = 'If set, there is no default radio button. '
      Caption = 'tfNoDefaultRadioButton'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 5
    end
    object cbPositionRelativeToWindow: TCheckBox
      Left = 15
      Top = 89
      Width = 150
      Height = 17
      Hint = 'If set, Task Dialog is centered with respect to parent window.'
      Caption = 'tfPositionRelativeToWindow'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 6
    end
    object cbShowProgressBar: TCheckBox
      Left = 167
      Top = 66
      Width = 123
      Height = 17
      Hint = 'If set, display the progress bar. '
      Caption = 'tfShowProgressBar'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 7
    end
    object cbCallbackTimer: TCheckBox
      Left = 476
      Top = 66
      Width = 97
      Height = 17
      Hint = 'If set, dialogs callback will be called every 200 milliseconds. '
      Caption = 'tfCallbackTimer'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 8
    end
    object cbShowMarqueeProgressBar: TCheckBox
      Left = 324
      Top = 66
      Width = 152
      Height = 17
      Hint = 'If set, display the marquee progress bar.'
      Caption = 'tfShowMarqueeProgressBar'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 9
    end
    object cbVerificationFlagChecked: TCheckBox
      Left = 15
      Top = 66
      Width = 137
      Height = 17
      Hint = 'If set, the verification check box is initially checked. '
      Caption = 'tfVerificationFlagChecked '
      ParentShowHint = False
      ShowHint = True
      TabOrder = 10
    end
    object cbUseCommandLinks: TCheckBox
      Left = 15
      Top = 43
      Width = 122
      Height = 17
      Hint = 
        'If set, buttons are displayed as command links using a standard ' +
        'dialog glyph. '
      Caption = 'tfUseCommandLinks '
      ParentShowHint = False
      ShowHint = True
      TabOrder = 11
    end
    object cbUseCommandLinksNoIcon: TCheckBox
      Left = 167
      Top = 43
      Width = 151
      Height = 17
      Hint = 'If set, buttons are displayed as command links without a glyph. '
      Caption = 'tfUseCommandLinksNoIcon'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 12
    end
    object cbExpandFooterArea: TCheckBox
      Left = 324
      Top = 43
      Width = 124
      Height = 17
      Hint = 'If set, display expanded text in the footer. '
      Caption = 'tfExpandFooterArea'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 13
    end
    object cbExpandedByDefault: TCheckBox
      Left = 476
      Top = 43
      Width = 129
      Height = 17
      Hint = 'If set, expanded text is displayed when the Task Dialog opens.'
      Caption = 'tfExpandedByDefault'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 14
    end
    object cbRtlLayout: TCheckBox
      Left = 167
      Top = 89
      Width = 113
      Height = 17
      Hint = 'If set, text reads right-to-left. '
      Caption = 'tfRtlLayout'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 15
    end
  end
  object cbIcons: TComboBox
    Left = 195
    Top = 311
    Width = 145
    Height = 21
    TabOrder = 16
    Text = 'tdiError'
    Items.Strings = (
      'tdiNone'
      'tdiWarning'
      'tdiError'
      'tdiInformation'
      'tdiShield')
  end
  object cbDefaultButton: TComboBox
    Left = 647
    Top = 311
    Width = 145
    Height = 21
    ItemIndex = 0
    TabOrder = 17
    Text = 'tcbOk'
    Items.Strings = (
      'tcbOk')
  end
  object cbVerification: TCheckBox
    Left = 168
    Top = 114
    Width = 81
    Height = 17
    Caption = 'Verification'
    TabOrder = 18
  end
  object cbRadioButtons: TCheckBox
    Left = 255
    Top = 115
    Width = 88
    Height = 17
    Caption = 'Radio Buttons'
    TabOrder = 19
  end
  object edtFooterText: TEdit
    Left = 248
    Top = 338
    Width = 544
    Height = 21
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 20
  end
  object cbFooterIcon: TComboBox
    Left = 418
    Top = 311
    Width = 145
    Height = 21
    TabOrder = 21
    Text = 'tdiError'
    Items.Strings = (
      'tdiNone'
      'tdiWarning'
      'tdiError'
      'tdiInformation'
      'tdiShield')
  end
  object edtMarquee: TEdit
    Left = 400
    Top = 111
    Width = 50
    Height = 21
    Anchors = [akTop, akRight]
    NumbersOnly = True
    TabOrder = 22
    Text = '10'
  end
  object cbProgressType: TComboBox
    Left = 647
    Top = 366
    Width = 145
    Height = 21
    ItemIndex = 0
    TabOrder = 23
    Text = 'pbsNormal'
    Items.Strings = (
      'pbsNormal'
      'pbsError'
      'pbsPaused')
  end
  object edtProgressPosition: TEdit
    Left = 513
    Top = 366
    Width = 50
    Height = 21
    Anchors = [akTop, akRight]
    NumbersOnly = True
    TabOrder = 24
    Text = '10'
  end
  object edtProgressMin: TEdit
    Left = 248
    Top = 364
    Width = 50
    Height = 21
    Anchors = [akTop, akRight]
    NumbersOnly = True
    TabOrder = 25
    Text = '0'
  end
  object edtProgressMax: TEdit
    Left = 382
    Top = 364
    Width = 50
    Height = 21
    Anchors = [akTop, akRight]
    NumbersOnly = True
    TabOrder = 26
    Text = '100'
  end
  object btnAPI: TButton
    Left = 16
    Top = 200
    Width = 97
    Height = 25
    Hint = 
      'The TaskDialog function is declared inside the Winapi.CommCtrl.p' +
      'as unit.'
    Caption = 'API'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 27
    OnClick = btnAPIClick
  end
  object btnProgress: TButton
    Left = 16
    Top = 231
    Width = 97
    Height = 25
    Caption = 'Progress Kompon'
    TabOrder = 28
    OnClick = btnProgressClick
  end
  object TaskDialog1: TTaskDialog
    Buttons = <>
    RadioButtons = <>
    OnHyperlinkClicked = TaskDialog1HyperlinkClicked
    OnNavigated = TaskDialog1Navigated
    Left = 24
    Top = 416
  end
end
