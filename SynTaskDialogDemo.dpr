program SynTaskDialogDemo;

uses
  Vcl.Forms,
  uFrmTestMormotDialog in 'uFrmTestMormotDialog.pas' {FrmTestMormotDialog},
  SynTaskDialog in 'SynTaskDialog.pas',
  uZamjenaSynTask in 'uZamjenaSynTask.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFrmTestMormotDialog, FrmTestMormotDialog);
  Application.Run;
end.
